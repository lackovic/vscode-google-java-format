# Google Java Format for Visual Studio Code

[google-java-format](https://github.com/google/google-java-format) extension for
Visual Studio Code.

## Prerequisites

* working Java installation

## Installation

1. download the [latest version of google-java-format all-deps jar](https://github.com/google/google-java-format/releases)

1. move the jar to a folder (e.g. `/opt/`)

1. enter the full path to the jar in the `gjf.jarPath` of your user or workspace settings as follows:

      ```
      "gjf.jarPath": "/opt/google-java-format-1.9-all-deps.jar"
      ```

## Known Issues

* Formatting of text sections only works for complete expressions

